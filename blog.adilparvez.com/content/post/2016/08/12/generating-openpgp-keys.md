{
  "Categories": [
    "Misc."
  ],
  "date": "2016-08-12T09:07:00",
  "title": "Generating OpenPGP keys"
}

We look at what makes up OpenPGP keys, and how to properly generate them with [GnuPG](https://gnupg.org/).

<!--more-->

## Key structure
A **key pair** consists of a public key and a private key.
An **OpenPGP key** consists of a **primary key pair** and zero or more **subkey pairs**, as well as associated metadata.

```
OpenPGP key {
    primary key pair {
        public
        private
    }
    subkey {
        public
        private
    }
    subkey {
        public
        private
    }
    ...
    metadata
}
```

A key can have the following roles (non-exclusive):

- **Encryption**
- **Authentication**
- **Signing** providing authenticity, non-repudiation, and integrity. Lets others verify a message came from you, you can not deny having sent it, and it was not modified in transit.
- **Certification**, signing keys.

A primary key must be able to certify.

Subkeys are related to the primary key pair by being certified by it.

## Generating keys
We should do this in a secure environment, e.g. air-gapped [tails](https://tails.boum.org/).

An overview of what we will do:

- Generate a RSA primary key pair that can only certify.
- Add subkeys for encryption, authentication, and signing.
- Backup keys and generate a revocation certificate.
- Remove the primary private key.

Some rationale for this is that if our keys are compromised, we wont lose the trust our primary key has built up, since we only need to revoke the compromised subkeys. Note the revocation certificate only applys to the primary key, revoke the subkeys under --edit-key.

### Generate
<div>
<div id="generate-key"></div>
<script>
  asciinema.player.js.CreatePlayer("generate-key", "/res/2016/08/12/generate-key.json");
</script>
</div>

The state of the keyring is
```
~ $ gpg --list-key
/home/x/.gnupg/pubring.gpg
--------------------------
pub   2048R/EB73C2D1 2016-06-25
uid                  First Last <first.last@example.tld>

~ $ gpg --list-secret-key
/home/x/.gnupg/secring.gpg
--------------------------
sec   2048R/EB73C2D1 2016-06-25
uid                  First Last <first.last@example.tld>
```

### Add subkeys
<div>
<div id="subkeys"></div>
<script>
  asciinema.player.js.CreatePlayer("subkeys", "/res/2016/08/12/subkeys.json");
</script>
</div>

The state of the keyring is
```
~ $ gpg --list-key
/home/x/.gnupg/pubring.gpg
--------------------------
pub   2048R/EB73C2D1 2016-06-25
uid                  First Last <first.last@example.tld>
sub   2048R/FEDFA9E8 2016-06-25
sub   2048R/386F33A1 2016-06-25
sub   2048R/DD7B67AE 2016-06-25

~ $ gpg --list-secret-key
/home/x/.gnupg/secring.gpg
--------------------------
sec   2048R/EB73C2D1 2016-06-25
uid                  First Last <first.last@example.tld>
ssb   2048R/FEDFA9E8 2016-06-25
ssb   2048R/386F33A1 2016-06-25
ssb   2048R/DD7B67AE 2016-06-25
```

### Backup and revokation certificate
<div>
<div id="revcert-and-backup"></div>
<script>
  asciinema.player.js.CreatePlayer("revcert-and-backup", "/res/2016/08/12/revcert-and-backup.json");
</script>
</div>

### Remove primary private key.
<div>
<div id="remove-private-key"></div>
<script>
  asciinema.player.js.CreatePlayer("remove-private-key", "/res/2016/08/12/remove-private-key.json");
</script>
</div>

The state of the keyring is
```
~ $ gpg --list-keys
/home/x/.gnupg/pubring.gpg
--------------------------
pub   2048R/EB73C2D1 2016-06-25
uid                  First Last <first.last@example.tld>
sub   2048R/FEDFA9E8 2016-06-25
sub   2048R/386F33A1 2016-06-25
sub   2048R/DD7B67AE 2016-06-25

~ $ gpg --list-secret-keys
/home/x/.gnupg/secring.gpg
--------------------------
sec#  2048R/EB73C2D1 2016-06-25
uid                  First Last <first.last@example.tld>
ssb   2048R/FEDFA9E8 2016-06-25
ssb   2048R/386F33A1 2016-06-25
ssb   2048R/DD7B67AE 2016-06-25

```
