---
Categories:
- Misc.
date: 2016-10-31T11:00:00
title: CyanogenMustardRace
---


An outline of my Android setup. Hint: Mind the Gapps.

<!--more-->

I'm currently on [CyanogenMod](http://www.cyanogenmod.org/), but may go over to an AOSP ROM sometime.

I don't flash GApps, but if you want to, I recommend [OpenGApps](http://opengapps.org/). However I do use [Signal](https://signal.org/) which needs play services for [GCM](https://developers.google.com/cloud-messaging/gcm), there used to be [LibreSignal](https://github.com/LibreSignal/LibreSignal) which could do the messaging component of Signal by using web sockets to connect directly to [OWS](https://whispersystems.org/)'s servers, but it is now [dead](https://github.com/LibreSignal/LibreSignal/issues/37#issuecomment-217211165). Thankfully [microG](https://microg.org/) lets us use GCM.

Almost all of my apps come from [F-Droid](https://f-droid.org/), I can't praise F-Droid and all of the open source developers enough, F-Droid is a treasure trove of high quality useful apps.

### Flashing CyanogenMod
This is device specific, check XDA for how to get [TWRP](https://twrp.me/) onto your device. Download [CyanogenMod](http://download.cyanogenmod.com/), boot into recovery and flash it.

Also flash [OpenGApps](http://opengapps.org/) before the first boot if you want to.

Some of the apps I use need root, enable this under devoloper options (tap build number 7 times) in CyanogenMod. For other ROMs, use [phh's SuperUser](http://forum.xda-developers.com/android/software-hacking/wip-selinux-capable-superuser-t3216394). Note: never use SuperSU, it is closed source and Chainfire sold it to [CCMT](https://www.reddit.com/r/Android/comments/54xdmp/ccmt_who_exactly_are_the_owners_of_supersu/).

Complete the initial setup and then install F-Droid.


### Xposed framework
[Xposed](http://repo.xposed.info/) is a framework for code injection, microG needs a module for signature spoofing, so it can pretend to be the official play services. Only use modules that you trust, they tend to be small so you can look over their code quickly.

Get Xposed [here](http://forum.xda-developers.com/showthread.php?t=3034811). Flash the framework zip in twrp, then install the [materialised installer](http://forum.xda-developers.com/xposed/material-design-xposed-installer-t3137758).

Install the [FakeGApps](http://repo.xposed.info/module/com.thermatk.android.xf.fakegapps) module.

### microG
Add the [microG repo](https://microg.org/fdroid.html) to F-Droid.

See [here](https://github.com/microg/android_packages_apps_GmsCore/wiki) for detailed instructions, the gist is to install the services core, gsf proxy, and fake store. Enable device check-in, and cloud messaging in microG settings. Finally reboot.

### Apps
All of these apps are available from F-Droid:

- AFWall+ (root): A frontend for iptables, keep it in whitelist mode, allowing only a few apps to connect to the internet. Great for restricting data to only Signal and email.

- Firefox: With uBlock Origin, HTTPS Everywhere and cookies disabled a saner browsing experience can be achieved.

- NewPipe: A YouTube client that works by parsing the website.

- AdAway (root): Block ads via the hosts file.

- [Silence](https://silence.im/): End-to-end encrypted SMS.

- VLC: Media player.

- Offline calendar: Make a local calendar that can be used by calendar apps.

- OpenKeychain: OpenPGP keychain.

- Document Viewer: Ebook reader.

- LibreTorrent: BitTorrent client.

- Materialistic: [HN](https://news.ycombinator.com/) client.

Enable the [Guardian Project](https://guardianproject.info/) repo to get:

- Orbot: Tor for Android.

- [ChatSecure](https://chatsecure.org/): End-to-end encrypted messaging app that can go over Tor.

Not on F-Droid:

- Signal: End-to-end encrypted messaging over the internet. (You can get it on [APKMirror](https://www.apkmirror.com/?s=signal&post_type=app_release&searchtype=apk).)
- [MAPS.ME](https://maps.me/en/home): Offline maps using [OSM](https://www.openstreetmap.org/) data. Download [here](https://maps.me/apk/).

Pro tips:

- There is a webapp for browsing the F-Droid repo called [Fossdroid](https://fossdroid.com/).

- You can use APKMirror to look for other apps you might want, there is also an APKMIrror [app](http://forum.xda-developers.com/android/apps-games/apkmirror-web-app-t3450564).


Edit: I neglected to mention that the first time you install Signal it should come from a trusted source (e.g. copy it from someone who is happy to use the play store), then updating via apkmirror is safe, since the signatures will match.

Also [Raccoon](http://www.onyxbits.de/raccoon) might be useful, a desktop play store client.
