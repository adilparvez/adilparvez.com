---
Categories:
- Misc.
date: 2016-11-13T11:00:00
title: vault
---


A tool for creating and managing dmcrypt+LUKS encrypted containers with a very simple UI.

<!--more-->
I wrote [vault](https://gitlab.com/adilparvez/vault) because the UX of using dd + cryptsetup + mkfs + mount isn't that great, vault is a python script that ties these together and exposes a simple and pleasant interface.

```bash
# Make a new encrypted container, initial data comes from /dev/urandom.
$ vault new foo 512MiB
Password:
Verify password:
Allocating file... [█████████████████████████] 100.00% Done.
Creating LUKS container... Done.
Creating file system... Done.
Mounted at: /home/x/foo-mnt

# List all open vaults with their ids.
$ vault list
0 -> /home/x/foo

# Close a vault with a particular id.
$ vault close --id 0

# Close a vault at a particular path.
$ vault close foo

# Open a vault.
$ vault open foo
Password:
Mounted at: /home/x/foo-mnt

# Close all open vaults.
$ vault close-all
```

See the code [here](https://gitlab.com/adilparvez/vault).
