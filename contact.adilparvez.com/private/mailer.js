"use strict";

var nodemailer = require('nodemailer');

module.exports = function (to) {
    var module = {};

    var transporter = nodemailer.createTransport({
        service: "<SERVICE>",
        auth: {
            user: to,
            pass: "<PASSWORD>"
        }
    });

    module.send = function (from, subject, text, callback) {
        var mailOptions = {
            to: to,
            from: from,
            subject: subject,
            text: text
        };
        transporter.sendMail(mailOptions, callback);
    };

    return module;
};

